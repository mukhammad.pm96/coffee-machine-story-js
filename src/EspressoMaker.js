class EspressoMaker {
    make() {
        return {
            espresso: 1,
        };
    }
}

export default EspressoMaker;
